<?php
function verifica_campo($texto){
    $texto = trim($texto);
    $texto = stripslashes($texto);
    $texto = htmlspecialchars($texto);
    return $texto;
}

$email = "";
$senha = "";
$nome_cadastro = "";
$email_cadastro = "";
$senha_cadastro = "";
$confirmacao_senha_cadastro = "";

$tem_erro = false;

//validar a parte login
if(isset($_GET['btn']) && $_GET['btn'] == 'entrar') { 
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if(isset($_POST['email']))
            $email = $_POST['email'];

        if(isset($_POST["senha"]))
            $senha = $_POST["senha"];
    
        if (!(filter_var($email, FILTER_VALIDATE_EMAIL))) {
            $erro_email = "Email incorreto";
            $tem_erro = true;
        }
        if(empty($email)) {
            $erro_email = "Digite seu email";
            $tem_erro = true;
        }
        if(empty($senha)) {
            $erro_senha = "Digite sua senha";
            $tem_erro = true;
        }
    }
    if(!$tem_erro) {
        $email = verifica_campo($email);
        $senha = verifica_campo($senha);
    
        header("Location: validacoes/validar_login.php?email=$email&senha=$senha");
    } 
//validar a parte cadastro
} else if (isset($_GET['btn']) && $_GET['btn'] == 'cadastrar') { 
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if(isset($_POST['nome-cadastro'])) {
            $nome_cadastro = $_POST['nome-cadastro'];
        }
        if(isset($_POST["email-cadastro"])) {
            $email_cadastro = $_POST["email-cadastro"];
        }
        if(isset($_POST["senha-cadastro"])) {
            $senha_cadastro = $_POST["senha-cadastro"];
        }
        if(isset($_POST["confirmacao-senha-cadastro"])) {
            $confirmacao_senha_cadastro = $_POST["confirmacao-senha-cadastro"];
        }
        if(empty($nome_cadastro)) {
            $erro_nome_cadastro = "Digite seu nome";
            $tem_erro = true;
        }
        if (!(filter_var($email_cadastro, FILTER_VALIDATE_EMAIL))) {
            $erro_email_cadastro = "Email incorreto";
            $tem_erro = true;
        }
        if(empty($email_cadastro)) {
            $erro_email_cadastro = "Digite seu email";
            $tem_erro = true;
        }
        if(empty($senha_cadastro)) {
            $erro_senha_cadastro = "Digite sua senha";
            $tem_erro = true;
        }
        if(empty($confirmacao_senha_cadastro)) {
            $erro_confirmacao_senha = "Digite sua senha";
            $tem_erro = true;
        }
        if($_POST["senha-cadastro"] != $_POST["confirmacao-senha-cadastro"]) {
            $erro_senhas_diferentes = "As senhas não são iguais";
            $tem_erro = true;
        } 
        
        if(!$tem_erro) {
            $nome_cadastro = verifica_campo($nome_cadastro);
            $email_cadastro = verifica_campo($email_cadastro);
            $senha_cadastro = verifica_campo($senha_cadastro);
            $confirmacao_senha_cadastro = verifica_campo($confirmacao_senha_cadastro);

        //lógica para realizar o cadastro
        header("Location: validacoes/realizar_cadastro.php?nome=$nome_cadastro&email=$email_cadastro&senha=$senha_cadastro");
        } 
    }
}

?>
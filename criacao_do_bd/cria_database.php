<?php 
require "../validacoes/credentials.php";

// Create connection
$conn = mysqli_connect($servername, $user, $password);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Create database
$sql = "CREATE DATABASE $dbname";
if (mysqli_query($conn, $sql)) {
    
} else {
    echo "Error creating database: " . mysqli_error($conn);
}

mysqli_close($conn);


// Create connection
$conn = mysqli_connect($servername, $user, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$nao_tem_erro = false;

// sql to create table
$sql = "CREATE TABLE usuario ( 
    codId BIGINT UNSIGNED AUTO_INCREMENT UNIQUE, 
    nome VARCHAR(50) NOT NULL, 
    email VARCHAR(50), 
    senha VARCHAR(50), 
    CONSTRAINT fkUsuario PRIMARY KEY (codId),
    CONSTRAINT ukEmail UNIQUE (email)
);";

if (mysqli_query($conn, $sql)) {
    $nao_tem_erro = true;
} else {
    echo "Error creating table: " . mysqli_error($conn);
}

$sql = "CREATE TABLE pets (
    codP BIGINT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE,
    codDono BIGINT UNSIGNED,
    nome varchar(40),
    categoria varchar(40),
    sexo char,
    raca varchar(40),
    idade SMALLINT,
    nascimento DATE,
    CONSTRAINT pkPet PRIMARY KEY (codP),
    CONSTRAINT fkPetDono FOREIGN KEY (codDono) REFERENCES usuario (codId)
    ON DELETE NO ACTION ON UPDATE CASCADE
    );";

if (mysqli_query($conn, $sql)) {
    $nao_tem_erro = true;
} else {
    echo "Error creating table: " . mysqli_error($conn);
}

if($nao_tem_erro) {
    header('location: ../login.php');
}

mysqli_close($conn);
?>
$sql = "CREATE TABLE pets (
    codP BIGINT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE,
    codDono BIGINT UNSIGNED,
    nome varchar(40),
    categoria varchar(40),
    sexo char,
    raca varchar(40),
    idade SMALLINT,
    nascimento DATE,
    CONSTRAINT pkPet PRIMARY KEY (codP),
    CONSTRAINT fkPetDono FOREIGN KEY (codDono) REFERENCES usuario (codId)
    ON DELETE NO ACTION ON UPDATE CASCADE
    );";

if (mysqli_query($conn, $sql)) {
    $nao_tem_erro = true;
} else {
    echo "Error creating table: " . mysqli_error($conn);
}

if($nao_tem_erro) {
    header('location: ../login.php');
}

mysqli_close($conn);
?>
<?php
require "validacoes/credentials.php";
require "validacoes/authenticate.php";
require "validacoes/check_form_pets.php";
// HEADER PARA EXCLUSÃO DE PET -->
if (isset($_GET['id']) && $_GET['btn'] == 'confirma_exclusao') {  
  header("location: validacoes/remove_pet.php?id=" . $_GET['id']);
}
?>
<!DOCTYPE html>
<html lang="PT-BR">
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous" />
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
  <script src="https://cdn.lordicon.com/lusqsztk.js"></script>
  <link rel="stylesheet" href="css/index.css" />
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&family=Press+Start+2P&family=Source+Sans+Pro:wght@200&display=swap" rel="stylesheet">
  <title>PadGree - Index</title>
</head>

<body>
    <div class="user-card pb-3">
      <?php 
      $conn = mysqli_connect($servername, $user, $password, $dbname);
      $sqli_usuario = "select nome from usuario where codID = $user_id";
      if (!($sqli_usuario = mysqli_query($conn, $sqli_usuario))) {
        echo "Problema ao selecionar nome do usuário na DB!";
      } else {
        $row = mysqli_fetch_assoc($sqli_usuario);
      }
      echo '<p class="mb-0">Olá ' . $row['nome'] . "!</p>" . '<span class="mb-2">Seja bem-vindo(a)!</span>';
      mysqli_close($conn);
      ?>    

    </div>
    <div class="m-2 m-md-5 mt-md-3">
    <!-- CABECALHO DAS TABS -->
    <div class="row">
      <div class="col-12 col-md-2">
      </div>
      <div class="col-12 col-md-8 d-flex justify-content-center pe-0">
        <ul class="nav nav-tabs border-0" id="myTab" role="tablist">
          <li class="nav-item border-0 me-2" role="presentation">
            <button class="nav-link active border-0" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">
              Minha Família
            </button>
          </li>
          <li class="nav-item border-0 ms-2" role="presentation">
            <button class="nav-link border-0" id="add-membro-tab" data-bs-toggle="tab" data-bs-target="#add-membro" type="button" role="tab" aria-controls="add-membro" aria-selected="false">
              Novo Membro
            </button>
          </li>
        </ul>
      </div>
      <div class="col-12 col-md-2 d-flex justify-content-center justify-content-md-end div-btn-logout">
        <a href="logout.php">
          <button class="btn-logout">Sair</button>
        </a>
      </div>
    </div>
<!-- TABS BOOTSTRAP -->
    <div class="tab-content mt-5" id="myTabContent">
    <!-- TAB - MINHA FAMÍLIA -->     
    <div class="tab-pane show active fade home" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="add-pet-button">
        </div>
        <div class="container row justify-content-center mt-5 show-pets">
          <?php
          // CHAMADA DOS CARDS DE PET CONFORME EXISTÊNCIA NO BD          
          $conn = mysqli_connect($servername, $user, $password, $dbname);
          $sqli = "select * from pets where codDono = $user_id";
          $pets = mysqli_query($conn, $sqli);
          if (!($pets)) {
            echo "Problemas para carregar tarefas do BD!<br>" .
              mysqli_error($conn);
          }
          if (mysqli_num_rows($pets) > 0) :
            while ($cards = mysqli_fetch_assoc($pets)) :
          ?>
              <div class="col-12 col-md-6">
                <div class="pet-card row d-flex">
                  <div class="pet-card-foto col-4">
                    <img src="img/img-perfil-pet.png" class="img-fluid bg-white" alt="">
                  </div>
                <div id="<?= 'id-' . $cards['codP']?>" class="pet-card-info col-5">
                <h1 class="nome-info"><?= $cards["nome"] ?></h1>
                <label>Dados do Pet</label>              
                <hr>                    
                <label>Categoria:</label>
                <p class="categoria-info"><?= $cards['categoria'] ?></p>
                <label>Idade:</label>
                <p class="idade-info"><?= $cards['idade'] ?></p>
                <label>Sexo:</label>
                <p class="sexo-info"><?= $cards['sexo'] ?></p>
                <label>Data de Nascimento:</label>
                <p class="nascimento-info"><?= $cards['nascimento'] ?></p>
                <label>Raça:</label>
                <p class="raca-info"><?= $cards['raca'] ?></p>
              </div>
                  <div class="row d-flex align-items-end pad-card-foot ">
                    <div class="col-2">
                        <button id="btn-excluir-<?=$cards["codP"]?>" onclick="pegarId(<?=$cards['codP']?>)" class="btn btn-edit-pet btn-cancel" type="submit" title="Excluir">
                          <lord-icon src="https://cdn.lordicon.com/qsloqzpf.json" trigger="hover" colors="primary:#e83a30" state="hover-empty" style="cursor:pointer;" data-bs-dismiss="modal">
                          </lord-icon>
                        </button>
                      </div>
                      <div class="col-2">
                        <button type="button" class="btn btn-edit-pet pega-id" onclick="pegarIdCadastrar(<?=$cards['codP']?>); preencherCampos(<?=$cards['codP']?>)" id="btn-editar-<?= $cards["codP"];?>" data-bs-toggle="modal" data-bs-target="#edit-pet">
                          <lord-icon src="https://cdn.lordicon.com/esmkrlag.json" trigger="hover" colors="primary:#121331" state="hover-2"></lord-icon>
                        </button>
                      </div>
                  </div>
                </div>
                </div>
                <?php
            endwhile;
          endif;
          mysqli_close($conn);
          // FINALIZANDO CONEXÃO COM BD E TÉRMINO DA CRIAÇÃO DINÂMICA DOS CARDS
          ?>
        </div>
      </div>
      <!-- TAB - CADASTRAR PET -->     
      <div class="tab-pane fade active" id="add-membro" role="tabpanel" aria-labelledby="add-membro-tab">
        <div id="form-add-pet" class="mt-5 mx-auto d-flex justify-content-center">
          <form id="form-cadastrar-pet" method="POST" class="form-sign-up" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>?btn=cadastrar_pet">
            <div class="mb-3">
              <label for="name-pet" class="form-label">Nome do seu pet:</label>
              <input type="text" class="form-control" name="name-pet" id="name-pet" value="<?= $nome ?>">
              <?php
              // SETANDO INFORMAÇÕES EM CAMPO ESPECÍFICO CASO HAJA ERRO - NOME
              if (isset($erro_nome))
                echo "<div style='color: #d9534f;
                    margin-bottom: 10px;
                    text-align: center;'> $erro_nome </div>";
              ?>
            </div>
            <div>
              <label for="especie-pet" class="form-label">O seu pet é:</label>
              <select onchange="selectCategoriaPet(this, '#campo-outro')" id="categoriaPet" name="categoria-pet" class="rounded">
                <option disabled selected>Selecione a categoria do seu pet</option>
                <option value="AVE">Ave</option>
                <option value="CACHORRO">Cachorro</option>
                <option value="COELHO">Coelho</option>
                <option value="GATO">Gato</option>
                <option value="HAMSTER">Hamster</option>
                <option value="PEIXE">Peixe</option>
                <option value="OUTRO">Outro...</option>
              </select>
            </div>
            <div id="campo-outro" class="d-none mb-3">
              <label for="outro-pet" class="form-label">Digite a categoria do seu pet:</label>
              <input type="text" class="form-control" name="outro-pet" id="outro-pet">
            </div>
            <?php
            if (isset($erro_categoria))
            // SETANDO INFORMAÇÕES EM CAMPO ESPECÍFICO CASO HAJA ERRO - CATEGORIA
            echo "<div style='color: #d9534f;
            margin-bottom: 10px;
            text-align: center;'> $erro_categoria </div>";
            ?>
            <div class="my-3">
              <p class="mb-0">Selecione o sexo do seu pet:</p>

              <div class="form-check">
                <input class="form-check-input" type="radio" name="sexo" value="F" id="macho-pet">
                <label class="form-check-label" for="macho-pet">
                  Fêmea
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="sexo" value="M" id="femea-pet">
                <label class="form-check-label" for="femea-pet">
                  Macho
                </label>
              </div>
            </div>
            <div class="mb-3">
              <label for="raca-pet" class="form-label">Raça do seu pet é:</label>
              <input type="text" class="form-control" name="raca-pet" id="raca-pet">
            </div>
            <div class="mb-3">
              <label for="idade-pet" class="form-label">Seu pet tem quantos anos?</label>
              <input type="number" class="form-control" name="idade-pet" id="idade-pet" value="0">
            </div>
            <div class="mb-3">
              <label for="aniver-pet" class="form-label">Data de nascimento do seu pet é:</label>
              <input type="date" class="form-control" name="aniver-pet" id="aniver-pet">
            </div>
            <div class="mb-3 d-flex justify-content-end">
              <button type="submit" class="btn-submit w-50">Adicionar Membro</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <footer>
      <div class=""></div>
    </footer>
  </div>
  <!-- MODAL PARA EDIÇÃO -->
  <!-- chama tela de edição com formulário -->
  <div class="modal fade" id="edit-pet" tabindex="-1" aria-labelledby="edit-pet-label" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content modal-bg border-0">
        <div class="modal-header no-border">
          <h5 class="modal-title" id="edit-pet-label">Editar Informações do Pet:</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <!-- FORMULARIO DE EDICAO -->
          <form id="form-editar-pet" method="POST" class="form-sign-up">
          <div class="mb-3">
              <label for="name-pet-editar" class="form-label">Nome do seu pet:</label>
              <input type="text" class="form-control" name="name-pet-editar" id="name-pet-editar">
            </div>
            <?php
            // SETANDO INFORMAÇÕES EM CAMPO ESPECÍFICO CASO HAJA ERRO - NOME-EDITAR
            if (isset($erro_nome_editar))
              echo "<div style='color: #d9534f;
              margin-top: 0px;
              text-align: center;'> $erro_nome_editar </div>";
            ?>
            <div>
              <label for="categoria-pet-editar" class="form-label">O seu pet é:</label>
              <select onchange="selectCategoriaPet(this, '#campo-outro-editar')" id="categoria-pet-editar" name="categoria-pet-editar" class="rounded">
                <option disabled selected>Selecione a categoria do seu pet</option>
                <option value="AVE">Ave</option>
                <option value="CACHORRO">Cachorro</option>
                <option value="COELHO">Coelho</option>
                <option value="GATO">Gato</option>
                <option value="HAMSTER">Hamster</option>
                <option value="PEIXE">Peixe</option>
                <option value="OUTRO">Outro...</option>
              </select>
              <script src="js/index.js"></script>
            </div>
            <div id="campo-outro-editar" class="d-none mb-3">
              <label for="outro-pet-editar" class="form-label">Digite a categoria do seu pet:</label>
              <input type="text" class="form-control" name="outro-pet-editar" id="outro-pet">
            </div>
            <?php
            // SETANDO INFORMAÇÕES EM CAMPO ESPECÍFICO CASO HAJA ERRO - CATEGORIA-EDITAR
            if (isset($erro_categoria_editar))
              echo "<div style='color: #d9534f;
              margin-bottom: 10px;
              text-align: center;'> $erro_categoria_editar </div>"
            ?>
            <div class="mb-3">
              <p class="mb-0">Selecione o sexo do seu pet:</p>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="sexo-editar" value="F" id="editar-macho-pet">
                <label class="form-check-label" for="editar-macho-pet">
                  Fêmea
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="sexo-editar" value="M" id="editar-femea-pet">
                <label class="form-check-label" for="editar-femea-pet">
                  Macho
                </label>
              </div>
            </div>
            <div class="mb-3">
              <label for="raca-pet-editar" class="form-label">A raça do seu pet é:</label>
              <input type="text" class="form-control" name="raca-pet-editar" id="raca-pet-editar">
            </div>
            <div class="mb-3">
              <label for="idade-pet-editar" class="form-label">Seu pet tem quantos anos?</label>
              <input type="number" class="form-control" name="idade-pet-editar" id="idade-pet-editar" value="0">
            </div>
            <div class="mb-3">
              <label for="aniver-pet-editar" class="form-label">A data de nascimento do seu pet é:</label>
              <input type="date" class="form-control" name="aniver-pet-editar" id="aniver-pet-editar">
            </div>
        </div>
        <div class="modal-footer d-flex justify-content-end no-border">
          <div>
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Salvar alterações</button>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
  <script src="js/index.js"></script>
  <?php
  // SCRIPT DE AUTOCLICK QUANDO HOUVER ERRO NO CADASTRAMENTO DO PET 
  if (isset($_GET['btn']) && $_GET['btn'] == 'cadastrar_pet') {  ?>
    <script>
      document.querySelector("#add-membro-tab").click();
    </script>  
  <?php }; ?> 

  <?php 
  if (isset($_GET['id'])) { ?>
    <script>
      console.log('teste');
      document.querySelector("#btn-editar-<?= $_GET['id'] ?>").click();
    </script>
      
  <?php }; ?>
</body>

</html>



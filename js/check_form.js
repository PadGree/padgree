$(function(){
    $("#form-sign-in").on("submit",function(){
      let emailInput = $("input[name='email']");
      let senhaInput = $("input[name='senha']");

      let temErro = false;
  
      $("#erro-email").html("");
      $("#erro-senha").html("");
  
      if(emailInput.val() == "" || emailInput.val() == null) {
        $("#erro-email").html("O email é obrigatório!");
        temErro = true;
      } 
  
      if(senhaInput.val() == "" || senhaInput.val() == null) {
        $("#erro-senha").html("A senha é obrigatória!");
        temErro = true;
      } 
  
      if(temErro) {
        return(false);
      }
  
      return(true);
    });

    $("#form-sign-up").on("submit",function(){
      let nomeCadastroInput = $("input[name='nome-cadastro']");
      let emailCadastroInput = $("input[name='email-cadastro']");
      let senhaCadastroInput = $("input[name='senha-cadastro']");
      let confirmacaoSenhaInput = $("input[name='confirmacao-senha-cadastro']");
      let temErro = false;
  
      $("#erro-nome-cadastro").html("");
      $("#erro-email-cadastro").html("");
      $("#erro-senha-cadastro").html("");
      $("#erro-confirmacao-senha").html("");

  
      if(nomeCadastroInput.val() == "" || nomeCadastroInput.val() == null) {
        $("#erro-nome-cadastro").html("O nome é obrigatório!");
        temErro = true;
      } 
  
      if(emailCadastroInput.val() == "" || emailCadastroInput.val() == null) {
        $("#erro-email-cadastro").html("O email  é obrigatório!");
        temErro = true;
      } 

      if(senhaCadastroInput.val() == "" || senhaCadastroInput.val() == null) {
        $("#erro-senha-cadastro").html("A senha  é obrigatória!");
        temErro = true;
      } 

      if(confirmacaoSenhaInput.val() == "" || confirmacaoSenhaInput.val() == null) {
        $("#erro-confirmacao-senha").html("A confirmação de senha é obrigatória!");
        temErro = true;
      } 
  
      if(temErro) {
        return(false);
      }
  
      return(true);
    });

    
  });
  
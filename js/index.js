// RECEBE CATEGORIA DO PET, MOSTRA AUTOMATICAMENTE O CAMPO "OUTRO" E ZERA VALOR SE O CAMPO NÃO FOR UTILIZADO 
function selectCategoriaPet(objeto, campo) {
    const botaoOutro = document.querySelector(campo);
    let categoriaPet = objeto.value;
    if(categoriaPet == 'OUTRO') {
        botaoOutro.className = "d-block mb-3"
    } else {
        botaoOutro.className = "d-none mb-3"
        document.querySelector("#outro-pet").value = ""
    }
}

// (PARA EXCLUSÃO) RECEBE ID DO PET, MANDA POR HEADER CONFORME CONFIRMAÇÃO DE EXCLUSÃO
function pegarId(id) {
    let confirmacao = confirm("Você tem certeza? Essa ação não pode ser desfeita :(")
    let idPet = id;
    if(confirmacao) {
        window.location.href = `index.php?id=${idPet}&btn=confirma_exclusao`;
    } else {
        return true
    }
}

// (PARA EDIÇÃO) REDIRECIONA PARA PÁGINA INDEX.PHP COM HEADER CONTENDO ID E FUNÇÃO DO BOTAO
function pegarIdCadastrar(id) {
    //preencherCampos(id)
    let idPet = id;
    let formulario = document.querySelector("#form-editar-pet");
    formulario.action = `index.php?id=${idPet}&btn=editar_pet`;
}

function preencherCampos(id) {
    let nomeInfo = document.querySelector(`#id-${id} .nome-info`).innerHTML;
    let categoriaInfo = document.querySelector(`#id-${id} .categoria-info`).innerHTML;
    let idadeInfo = document.querySelector(`#id-${id} .idade-info`).innerHTML;
    let sexoInfo = document.querySelector(`#id-${id} .sexo-info`).innerHTML;
    let nascimentoInfo = document.querySelector(`#id-${id} .nascimento-info`).innerHTML;
    let racaInfo = document.querySelector(`#id-${id} .raca-info`).innerHTML;
    
    let nomeEditar = document.querySelector('#name-pet-editar')
    let categoriaEditar = document.querySelector("#categoria-pet-editar");
    let idadeEditar = document.querySelector("#idade-pet-editar");
    let sexoEditar;

    if(sexoInfo == 'F') {
        sexoEditar = document.querySelector("#editar-macho-pet");
    } else if (sexoInfo == 'M') {
        sexoEditar = document.querySelector("#editar-femea-pet");
    }
    let nascimentoEditar = document.querySelector("#aniver-pet-editar");
    let racaEditar = document.querySelector("#raca-pet-editar");

    nomeEditar.value = nomeInfo;
    categoriaEditar.value = categoriaInfo;
    idadeEditar.value = idadeInfo;
    sexoEditar.checked = true;
    nascimentoEditar.value = nascimentoInfo;
    racaEditar.value = racaInfo;
}
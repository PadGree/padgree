<?php require("check_form.php"); ?>
<!DOCTYPE html>
<html lang="PT-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login - Projeto WEB 2</title>
    <link rel="stylesheet" href="css/logins.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="js/check_form.js"></script>
</head>

<body>
    <div class="cont">
        <!-- FORMULÁRIO DE LOGIN -->
        <form id="form-sign-in" method="POST" class="form sign-in" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>?btn=entrar">
            <div>
                <h2>Bem vindo!</h2>
                <?php
                if (isset($_GET['erro']) && $_GET['erro'] == 'erro_user_data') : ?>
                    <div style="border-left: 3px solid red; text-align: center; padding: 10px 5px; 
                                margin: 10px 0 0 0; color: red; background-color: #ffe8e8">
                        Nome de usuário ou senha incorreta
                    </div>
                <?php endif ?>
                <label>
                    <span>Email:</span>
                    <input name="email" type="text" value="<?= $email ?>" />
                    <div id="erro-email" class="t-danger"></div>
                    <?php
                    if (isset($erro_email))
                        echo "<div class = 't-danger'> $erro_email </div>";
                    ?>
                </label>
                <label>
                    <span>Senha:</span>
                    <input name="senha" type="password" value="<?= $senha ?>" />
                    <div id="erro-senha" class="t-danger"></div>
                    <?php
                    if (isset($erro_senha))
                        echo "<div class = 't-danger'> $erro_senha </div>";
                    ?>
                </label>
                <button type="submit" class="submit">Entrar</button>
            </div>
        </form>

        <div class="sub-cont">
            <div>
                <div class="img">
                    <div class="img__text m--up">
                        <h2>Novo por aqui?</h2>
                        <div>Entre agora para nossa família e faça seu pet mais feliz!!</div>
                    </div>
                    <div class="img__text m--in">
                        <h2>Já faz parte da equipe?</h2>
                        <p>Se você já possui uma conta, entre. Nós sentimos sua falta!</p>
                    </div>
                    <div class="img__btn">
                        <span class="m--up">Entrar para a família</span>
                        <span class="m--in">Entrar no site</span>
                    </div>

                </div>
            </div>
            <!-- FORMULÁRIO DE CADASTRO -->
            <form id="form-sign-up" method="POST" class="form sign-up" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>?btn=cadastrar">
                <div>
                    <h2>Faça parte da família,</h2>
                    <?php
                    // MENSAGEM DE ERRO CASO EMAIL JÁ EXISTA
                    if (isset($_GET['erro']) && $_GET['erro'] == 'email_cadastrado') {
                        echo '<div style="border-left: 3px solid red; text-align: center; padding: 10px 5px; 
                        margin: 10px 0 0 0; color: red; background-color: #ffe8e8">
                            Esse email já foi cadastrado
                        </div>';
                    } ?>
                    <label>
                        <span>Nome</span>
                        <input name="nome-cadastro" type="text" value="<?= $nome_cadastro ?>" />
                        <div id="erro-nome-cadastro" class="t-danger"></div>
                        <?php
                        if (isset($erro_nome_cadastro))
                            echo "<div class = 't-danger'> $erro_nome_cadastro </div>";
                        ?>
                    </label>
                    <label>
                        <span>Email</span>
                        <input name="email-cadastro" type="email" value="<?= $email_cadastro ?>" />
                        <div id="erro-email-cadastro" class="t-danger"></div>
                        <?php
                        if (isset($erro_email_cadastro))
                            echo "<div class = 't-danger'> $erro_email_cadastro </div>";
                        ?>
                    </label>
                    <label>
                        <span>Senha</span>
                        <input name="senha-cadastro" type="password" value="<?= $senha_cadastro ?>" />
                        <div id="erro-senha-cadastro" class="t-danger"></div>
                        <?php
                        if (isset($erro_senha_cadastro))
                            echo "<div class = 't-danger'> $erro_senha_cadastro </div>";
                        ?>
                    </label>
                    <label>
                        <span>Confirmar Senha</span>
                        <input name="confirmacao-senha-cadastro" type="password" value="<?= $confirmacao_senha_cadastro ?>" />
                        <div id="erro-confirmacao-senha" class="t-danger"></div>
                        <?php
                        // MENSAGEM DE ERRO CASO SENHAS NÃO SEJAM IGUAIS OU CAMPOS NÃO TENHAM SIDO PREENCHIDOS
                        if (isset($erro_confirmacao_senha))
                            echo "<div class = 't-danger'> $erro_confirmacao_senha </div>";
                        else if (isset($erro_senhas_diferentes)) {
                            echo "<div class = 't-danger'> $erro_senhas_diferentes </div>";
                        }
                        ?>
                    </label>
                    <button type="submit" class="submit">Cadastrar</button>
                </div>
            </form>
        </div>
    </div>

    <?php
    if (isset($_GET['btn']) && $_GET['btn'] == 'cadastrar') {  ?>
        <script>
            document.querySelector(".cont").classList.toggle("s--signup");
        </script>
    <?php }; ?>
</body>
<script src="js/login.js"></script>
</html>
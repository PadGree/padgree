# Trabalho Final Desenvolvimento Web I - PadGree

![Badge em Desenvolvimento](http://img.shields.io/static/v1?label=STATUS&message=EM%20DESENVOLVIMENTO&color=GREEN&style=for-the-badge)

  

Esse projeto consiste em um sistema de gerenciamento de pets, onde o dono(a) pode adicionar seus pets ao sistema com suas respectivas informações e assim gerenciar todos os pets de sua família :wink:

  

## Desenvolvedores :computer:

Matheus Antonio Kerscher [![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/MatheusKerscher)

Nathan dos Santos Rosa [![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/ns-rosa)

Rafael Dias Simeoni [![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/RafaelSimeoni)

  

#

  

## :construction: Passo a passo para usar nosso sistema

1. Executar o arquivo ``cria_database.php`` que se encontra na pasta ``criacao_db_db``

2. Executar o arquivo ``login.php`` para poder começar a mexer na interface do sistema;

3. Agora é só mexer no sistema. Divirta-se :smiley:

  

#

  

### :wrench: Tecnologias utilizadas

  

- <a  href="https://getbootstrap.com/docs/5.0/getting-started/introduction/">``Bootstrap 5``</a> <img  align="center"  alt="logo Bootstrap"  height="30"  width="40"  src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/bootstrap/bootstrap-original.svg" />

- <a  href="https://developer.mozilla.org/pt-BR/docs/Web/CSS">``CSS 3``</a> <img  align="center"  alt="logo CSS3"  height="30"  width="40"  src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/css3/css3-original.svg" />


- <a  href="https://developer.mozilla.org/en-US/docs/Glossary/HTML5">``HTML 5``</a> <img  align="center"  alt="logo HTML5"  height="30"  width="40"  src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">

- <a  href="https://developer.mozilla.org/pt-BR/docs/Web/JavaScript">``Java Script``</a> <img  align="center"  alt="logo JavaScript"  height="30"  width="40"  src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">

- <a  href="https://api.jquery.com/">``jQuery``</a> <img height="30"  width="40" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/jquery/jquery-plain-wordmark.svg" />

- <a  href="https://lordicon.com/">``LordIcon``</a>

- <a  href="https://www.php.net/">``PHP``</a> <img  align="center"  alt="logo PHP"  height="40"  width="40"  src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/php/php-original.svg" />

#

  

### :hammer: Funcionalidades do projeto

  

- `Realizar Cadastro no Sistema`: Nesta tela o usuário poderá realizar seu cadastro em nosso sistema preenchendo o *formulário de cadastro*;

<img  alt="Tela de Cadastro"  src="/img/tela-cadastro.png" />

  

- `Realizar Login no Sistema`: Nesta tela o usuário consegue inserir seu *email* e sua *senha* para entrar em sua conta;

<img  alt="Tela de Login"  src="/img/tela-login.png" />

  

- `Adicionar seus pets a família`: Nesta tela o(a) dono(a) deverá preencher o *formulário de cadastro* com as informações de seu pet, para cadastrar ele em sua família;

<img  alt="Tela de Cadastar Pet"  src="/img/tela-cadastro-pet.png" />

  

- `Gerenciar os pets da sua família`: Nesta tela o(a) dono(a) poderá visualizar os pets que fazem parta da sua família, podendo *excluir* ou *editar informações* dos seus pets.

<img  alt="Tela Principal"  src="/img/tela-home.png" />

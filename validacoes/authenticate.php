<?php
  session_start();

  if (isset($_SESSION["user_id"])) {
    $login = true;
    $user_id = $_SESSION["user_id"];
  }
  else{
    $login = false;
    // CASO USUÁRIO NÃO ESTEJA LOGADO, SERÁ REDIRECIONADO PARA PÁGINA DE LOGIN!
    header("Location: login.php?erro=erro_user_data");
  }
?>
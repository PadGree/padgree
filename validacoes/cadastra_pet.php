<?php
    include "credentials.php";
    require "authenticate.php";

    function verifica_campo($texto)
    {
      $texto = trim($texto);
      $texto = stripslashes($texto);
      $texto = htmlspecialchars($texto);
      return $texto;
    }

    $conn = mysqli_connect($servername,$user,$password,$dbname);
    // RECEBENDO PARÂMETROS PELO HEADER E ALOCANDO EM VARIÁVEIS
    $nome = verifica_campo($_GET['nome']);
    $categoria = verifica_campo($_GET['categoria']);
    $sexo = verifica_campo($_GET['sexo']);
    $raca = verifica_campo($_GET['raca']);
    $idade = verifica_campo($_GET['idade']);
    $nascimento = verifica_campo($_GET['nascimento']);

    $sql = "INSERT INTO pets (codDono, nome, categoria, sexo, raca, idade, nascimento)  
    VALUES ( '$user_id', '$nome', '$categoria', '$sexo', '$raca', '$idade', '$nascimento');";
    $result = mysqli_query($conn, $sql);
    mysqli_close($conn);

    if($result){        
        header("Location: ../index.php?msg=sucesso-cadastro");
    } else {
        $error_msg = mysqli_error($conn);
        $error = true;
        die("Ocorreu um erro com o cadastramento do seu pet.");
      }
?>
<?php

function verifica_campo($texto)
{
    $texto = trim($texto);
    $texto = stripslashes($texto);
    $texto = htmlspecialchars($texto);
    return $texto;
}

$nome = "";
$categoria = "";
$sexo = "";
$raca = "";
$idade = "";
$nascimento = "";
$outro = "";

$nome_editar = "";
$categoria_editar = "";
$sexo_editar = "";
$raca_editar = "";
$idade_editar = 0;
$nascimento_editar = "";
$outro_editar = "";
$id_pet = "";
$tem_erro = false;

//validar o cadastro de pets
if (isset($_GET['btn']) && $_GET['btn'] == 'cadastrar_pet') {
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST['name-pet']))
            $nome = $_POST['name-pet'];

        if (isset($_POST["categoria-pet"]))
            if ($_POST["categoria-pet"] == "OUTRO") {
                $categoria = $_POST["outro-pet"];
            } else {
                $categoria = $_POST["categoria-pet"];
            }

        if (isset($_POST['raca-pet']))
            $raca = $_POST['raca-pet'];


        if (isset($_POST['sexo'])) {
            if ($_POST['sexo'] == 'M') {
                $sexo = 'M';
            } else if ($_POST['sexo'] == 'F') {
                $sexo = 'F';
                }
            }
        }

        if (isset($_POST['idade-pet']))
            $idade = $_POST['idade-pet'];

        if (isset($_POST['aniver-pet']))
            $nascimento = $_POST['aniver-pet'];

        if (empty($nome)) {
            $erro_nome = "Digite o nome do seu pet";
            $tem_erro = true;
        }
        if (empty($categoria)) {
            $erro_categoria = "Selecione a categoria do seu pet";
            $tem_erro = true;
        }

        if (empty($outro)) {
            if (($categoria) == 'OUTRO') {
                $erro_categoria = "Selecione a categoria do seu pet (erro outro)";
                $tem_erro = true;
            }
        }

    if (!$tem_erro) {
        $nome = verifica_campo($nome);
        $categoria = verifica_campo($categoria);
        $sexo = verifica_campo($sexo);
        $raca = verifica_campo($raca);
        $idade = verifica_campo($idade);
        $nascimento = verifica_campo($nascimento);
        $outro = verifica_campo($outro);

        //lógica para realizar o login
        header("Location: validacoes/cadastra_pet.php?nome=$nome&categoria=$categoria&sexo=$sexo&raca=$raca&idade=$idade&nascimento=$nascimento");
    }
    //validar a parte cadastro
} else if (isset($_GET['btn']) && $_GET['btn'] == 'editar_pet') {

    if ($_SERVER["REQUEST_METHOD"] == "POST") {


        if (empty($_POST['name-pet-editar'])) {
            $erro_nome_editar = "Digite o nome do seu pet";
            $tem_erro = true;
        } else {
            $nome_editar = $_POST['name-pet-editar'];
        }

        if (empty($_POST["categoria-pet-editar"]) || ($_POST["categoria-pet-editar"] == 'OUTRO' && empty($_POST["outro-pet-editar"]))){
            $erro_categoria_editar = "Selecione a categoria do seu pet!";
            $tem_erro = true;
        } else {
            if ($_POST["outro-pet-editar"] == "OUTRO") {
                    $categoria_editar = $_POST["outro-pet-editar"];
                } else {
                    $categoria_editar = $_POST["categoria-pet-editar"];
                }
        }

        if (isset($_POST['raca-pet-editar'])){
            $raca_editar = $_POST['raca-pet-editar'];
        }

        if (isset($_POST['sexo-editar'])) {
            if($_POST['sexo-editar'] == 'M') {
                $sexo_editar = 'M';
            } else if ($_POST['sexo-editar'] == 'F') {
                $sexo_editar = 'F';
            }
        }

        if(isset($_POST['idade-pet-editar']))
            $idade_editar = $_POST['idade-pet-editar'];

        if (isset($_POST['aniver-pet-editar']))
            $nascimento_editar = $_POST['aniver-pet-editar'];

        $id_pet = $_GET['id'];

        if (!$tem_erro) {
            $nome_editar = verifica_campo($nome_editar);
            $categoria_editar = verifica_campo($categoria_editar);
            $sexo_editar = verifica_campo($sexo_editar);
            $raca_editar = verifica_campo($raca_editar);
            $idade_editar = verifica_campo($idade_editar);
            $nascimento_editar = verifica_campo($nascimento_editar);
            $outro_editar = verifica_campo($outro_editar);
            $id_pet = verifica_campo($id_pet);

            //mandar para visualização de página
            header("Location: validacoes/edita_pet.php?id=$id_pet&nome=$nome_editar&categoria=$categoria_editar&sexo=$sexo_editar&raca=$raca_editar&idade=$idade_editar&nascimento=$nascimento_editar");
        }
    }
}

<?php
    include "credentials.php";
    include "authenticate.php";

    function verifica_campo($texto)
    {
        $texto = trim($texto);
        $texto = stripslashes($texto);
        $texto = htmlspecialchars($texto);
        return $texto;
    }
    
    // REMOVE PETS CONFORME PASSAGEM DO HEADER
    $conn = mysqli_connect($servername,$user,$password,$dbname);
    $id = $_GET['id'];
    $id = verifica_campo($id);
    // EXCLUSÃO UTILIZANDO CLÁUSULA WHERE
    $sql = "DELETE FROM pets WHERE codP = $id ";

    if($result = mysqli_query($conn, $sql)) {
        header('Location: ../index.php');
    } else {
        die('Ocorreu um problema com a exclusão!');
    }
    mysqli_close($conn);    
?>